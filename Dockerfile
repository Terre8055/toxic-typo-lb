FROM maven:3.6.3-openjdk-8 AS stage1
ENV MAVEN_OPTS="-XX:+TieredCompilation -XX:TieredStopAtLevel=1"
WORKDIR /opt/demo
COPY pom.xml .
COPY settings.xml .
RUN mvn -s settings.xml dependency:go-offline
COPY ./src ./src
RUN mvn -s settings.xml clean verify -Dmaven.test.skip=true

FROM eclipse-temurin:11
WORKDIR /opt/demo
COPY --from=stage1 /opt/demo/target/*.jar /opt/demo
CMD ["java", "-jar", "/opt/demo/toxictypoapp-1.0-SNAPSHOT.jar"] 