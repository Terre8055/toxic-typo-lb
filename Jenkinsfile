pipeline {
    agent any

    parameters {
        string(name: 'major_minor_version', defaultValue: '1.0', description: 'The major.minor version for release.')
    }

    options {
        timestamps()
        timeout(time: 15, unit: 'MINUTES') 
        gitLabConnection('gitlab')
    }

    environment {
        AWS_ECR = "644435390668.dkr.ecr.ap-south-1.amazonaws.com"
        RUNTIME_ENV_IP = "hodesay.servebeer.com"
        RUNTIME_ENV_USER = "ec2-user@${RUNTIME_ENV_IP}"
        RUNTIME_TEST_PORT = "8091"
        DOCKER_IMAGE = "toxic-typo-lb-mike"
        QA_DNS = "http://mike-alb-toxic-1277175235.ap-south-1.elb.amazonaws.com:8020/api/name"
        TARGET_GROUP_PORT = "8050" 
    }

    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')    
    }

    stages {

        stage ('Checkout') {
            steps {
                checkout scm
            }
        }


        stage('Build') {
            steps {
                script {
                    try {
                        sh "echo *************Building"
                        configFileProvider([configFile(fileId: 'artifactory-settings', variable: 'MAVEN_SETTINGS_XML')]) {
                            sh 'docker build -t ${DOCKER_IMAGE} .'
                        }
                    } catch (Exception buildError) {
                        echo "Build failed: ${buildError.message}"
                        currentBuild.result = 'FAILURE'
                        error "Build failed."
                    }
                }
            }
        }

        stage('Test') {
            steps {
                script {
                    try {
                        sh '''
                        echo "***********Running Tests" 
                        docker stop ${DOCKER_IMAGE} || true
                        docker rm ${DOCKER_IMAGE} || true
                        docker run --publish=8091:8080 -d --name=${DOCKER_IMAGE} ${DOCKER_IMAGE}:latest
                        sleep 5s
                        docker ps
                        docker network ls
                        docker network connect jenkins-setup_ci_net ${DOCKER_IMAGE}
                        sleep 3
                        docker exec ${DOCKER_IMAGE} curl -v http://${RUNTIME_ENV_IP}:${RUNTIME_TEST_PORT}/api/name
                        docker stop ${DOCKER_IMAGE}
                        docker rm ${DOCKER_IMAGE}
                        '''
                    } catch (Exception testError) {
                        echo "Test failed: ${testError.message}"
                        currentBuild.result = 'FAILURE'
                        error "Test failed."
                    }
                }
            }
        }

        stage("Deploy") {
            steps {
                script {
                    def deployVersion = 1.0

                    echo "Deploying version: ${deployVersion}"

                    sh("aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin ${AWS_ECR}")
                    sh("docker tag ${DOCKER_IMAGE}:latest ${AWS_ECR}/${DOCKER_IMAGE}:${deployVersion}")
                    sh("docker push ${AWS_ECR}/${DOCKER_IMAGE}:${deployVersion}")
                    sh 'docker image rm ${DOCKER_IMAGE}:latest'
                }
            }
        }

        stage ('QA Server 1'){
            steps {
                script {
                        try {

                        def deployVersion = "1.0"
                        
                        
                        sh "echo ***********Running QA Tests on 1" 
                        sh "ssh -i /var/jenkins_home/confion-ec2-ssh-mike.pem -o 'StrictHostKeyChecking no' ec2-user@ec2-3-110-156-161.ap-south-1.compute.amazonaws.com"
                        sh "aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin ${AWS_ECR}"
                        sh "sleep 5s"
                        sh "docker pull ${AWS_ECR}/${DOCKER_IMAGE}:1.0"
                        sh "sleep 5s"
                        sh "docker run -d -p ${TARGET_GROUP_PORT}:8080 --name ${DOCKER_IMAGE} ${AWS_ECR}/${DOCKER_IMAGE}:1.0"
                        sh "sleep 30s"
                        sh "docker ps"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "docker stop ${DOCKER_IMAGE}"
                        sh "docker rm ${DOCKER_IMAGE}"
                        

                    } catch (Exception testError) {
                        echo "Test failed: ${testError.message}"
                        currentBuild.result = 'FAILURE'
                        error "Test failed."
                    }
                }
            }
        }

        stage ('QA Server 2'){
            steps {
                script {
                        try {

                        def deployVersion = "1.0"
                        
                        
                        sh "echo ***********Running QA Tests on 2" 
                        sh "ssh -i /var/jenkins_home/onana-spid.pem -o 'StrictHostKeyChecking no' ec2-user@ec2-13-127-23-149.ap-south-1.compute.amazonaws.com"
                        sh "aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin ${AWS_ECR}"
                        sh "sleep 5s"
                        sh "docker pull ${AWS_ECR}/${DOCKER_IMAGE}:1.0"
                        sh "sleep 5s"
                        sh "docker run -d -p  ${TARGET_GROUP_PORT}:8080 --name ${DOCKER_IMAGE} ${AWS_ECR}/${DOCKER_IMAGE}:1.0"
                        sh "sleep 15s"
                        sh "docker ps"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "curl ${QA_DNS} -d name=Mike"
                        sh "docker stop ${DOCKER_IMAGE}"
                        sh "docker rm ${DOCKER_IMAGE}"
                    } catch (Exception testError) {
                        echo "Test failed: ${testError.message}"
                        currentBuild.result = 'FAILURE'
                        error "Test failed."
                    }
                }
            }
        }

        stage("Report") {
            steps {
                script {
                    echo "***********Sending reports"

                    def recipients = emailextrecipients([culprits(), upstreamDevelopers(), developers()])

                    if (recipients){
                        emailext subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS',
                            body: '''$PROJECT_NAME - $BUILD_NUMBER: $BUILD_STATUS
                                    Here is a link to the logs $BUILD_URL''',
                            from: 'jenkins',
                            to: recipients
                    }else{
                        echo 'All good***Skipping culprits'
                    }
                }
            }
        }
    }
    

    post {

        always {
            deleteDir()
        }

        success {
            echo 'Build and deploy successful'
            
        }
        failure {
            echo 'Build and deploy failed'
            
        }

    }
}
